"use strict";
let name = '', age = '';
while (name == '' || name == null || age == '' || age == null || isNaN(age)) {
    name = prompt("What's your name?", name);
    age = prompt("How old are you?", age);
}

const welcome = `Welcome, ${name}`;
const forbidden = "You are not allowed to visit this website";

function showMessage (text) {
    alert(text);
}

if (age < 18) {
    showMessage (forbidden);
} else if (age <= 22) {
    let isAgree = confirm("Are you sure you want to continue?");
    if (isAgree) {
        showMessage (welcome);
    } else {
        showMessage (forbidden);
    }
} else {
    showMessage (welcome);
}

